import urllib.request as urllib3
import sys
import ssl

if len(sys.argv) != 5:
    print("""Use: Run with commandline arguments (1) base URL (2) first index (3) file extension (4) number of pages,
          e.g. 01_image-retrieval.py https://www.google.com/images/book_name_ 0 jpg 310""")

else:
    # Configure command line arguments
    base_url = sys.argv[1]
    i = int(sys.argv[2])
    extension = sys.argv[3]
    num_digits = len(sys.argv[4])

    # Create an SSL certificate
    ssl._create_default_https_context = ssl._create_unverified_context

    # Iteratively save all images with that base URL followed by a number, e.g. e-book pages
    while True:
        i_string = str(i).zfill(num_digits)
        print(('' + base_url + i_string + '.' + extension))
        image = urllib3.urlretrieve(('' + base_url + i_string + '.' + extension), ('saved_files/' + i_string + '.' + extension))
        i += 1
